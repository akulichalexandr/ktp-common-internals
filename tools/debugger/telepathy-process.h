/*
    Copyright (C) 2011 Collabora Ltd. <info@collabora.com>
      @author George Kiagiadakis <george.kiagiadakis@collabora.com>

    Copyright (C) 2016 Alexandr Akulich <akulichalexander@gmail.com>

    This library is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation; either version 2.1 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TELEPATHY_PROCESS_H
#define TELEPATHY_PROCESS_H

#include <QObject>

#include <TelepathyQt/DebugReceiver>
#include <TelepathyQt/PendingOperation>
#include <TelepathyQt/Types>

class TelepathyProcess : public QObject
{
    Q_OBJECT
public:
    enum Status {
        Disconnected,
        ErrorOccurred,
        EnablingMonitor,
        FetchingMessages,
        Ready
    };

    enum Error {
        NoError,
        UnknownError,
        NotSupported
    };

    explicit TelepathyProcess(QObject *parent = nullptr);
    ~TelepathyProcess();

    QString ownerId() const Q_REQUIRED_RESULT { return m_owner; }
    void setOwnerId(const QString &owner);

    QStringList names() const Q_REQUIRED_RESULT { return m_names; }
    void addName(const QString &name);
    void setNames(const QStringList &names);

Q_SIGNALS:
    void newDebugMessage(const Tp::DebugMessage &message);
    void namesChanged(const QStringList &names);
    void statusChanged(TelepathyProcess::Status status, TelepathyProcess::Error error);

protected Q_SLOTS:
    void onDebugReceiverInvalidated(Tp::DBusProxy *proxy,
            const QString &errorName, const QString &errorMessage);
    void onDebugReceiverReady(Tp::PendingOperation *op);
    void onDebugReceiverMonitoringEnabled(Tp::PendingOperation *op);
    void onFetchMessagesFinished(Tp::PendingOperation *op);
    void onNewDebugMessage(const Tp::DebugMessage &msg);

protected:
    void appendMessage(const Tp::DebugMessage &msg);
    void setStatus(Status status, Error error = NoError);

private:
    QString m_owner;
    QStringList m_names;
    Tp::DebugReceiverPtr m_debugReceiver;
    Tp::DebugMessageList m_tmpCache;
    Status m_status;
};

#endif // TELEPATHY_PROCESS_H
