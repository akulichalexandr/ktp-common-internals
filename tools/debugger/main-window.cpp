/*
    Copyright (C) 2011 Collabora Ltd. <info@collabora.com>
      @author George Kiagiadakis <george.kiagiadakis@collabora.com>

    This library is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation; either version 2.1 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "main-window.h"
#include "debug-message-view.h"
#include "telepathy-process.h"

#include <QDBusConnection>
#include <QDBusConnectionInterface>
#include <QDBusPendingCallWatcher>
#include <QDBusServiceWatcher>
#include <QDBusPendingReply>

#include <QAction>
#include <QStatusBar>
#include <KToolBar>
#include <KLocalizedString>

static const QLatin1String s_telepathyServicePrefix("org.freedesktop.Telepathy.");

MainWindow::MainWindow(QWidget *parent)
    : KXmlGuiWindow(parent)
{
    setCentralWidget(new QWidget(this));
    m_ui.setupUi(centralWidget());
    setupGUI();

    QDBusConnection connection = QDBusConnection::sessionBus();

    if (!connection.isConnected()) {
        qDebug() << "Failed to connect to DBus";
        return;
    }

    connection.connect(QStringLiteral("org.freedesktop.DBus"),
                       QStringLiteral("/org/freedesktop/DBus"),
                       QStringLiteral("org.freedesktop.DBus"),
                       QStringLiteral("NameOwnerChanged"),
                       QStringList(), QString(),
                       this, SLOT(onServiceOwnerChanged(QString, QString, QString)));

    QDBusPendingCall async = QDBusConnection::sessionBus().interface()->asyncCall(QLatin1String("ListNames"));
    QDBusPendingCallWatcher *callWatcher = new QDBusPendingCallWatcher(async, this);
    connect(callWatcher, SIGNAL(finished(QDBusPendingCallWatcher*)),
            this, SLOT(serviceNameFetchFinished(QDBusPendingCallWatcher*)));

    QAction *saveLogAction = new QAction(QIcon::fromTheme(QLatin1String("document-save-as")), i18n("Save Log"), this);
    saveLogAction->setToolTip(i18nc("Toolbar icon tooltip", "Save log of the current tab"));
    toolBar()->addAction(saveLogAction);

    connect(saveLogAction, SIGNAL(triggered(bool)), this, SLOT(saveLogFile()));
}

MainWindow::~MainWindow()
{

}

void MainWindow::saveLogFile()
{
    m_ui.tabWidget->currentWidget()->findChild<DebugMessageView *>()->saveLogFile();
}

void MainWindow::onServiceOwnerChanged(const QString &name, const QString &oldOwner, const QString &newOwner)
{
    qDebug() << Q_FUNC_INFO << name << oldOwner << newOwner;

    if (!name.startsWith(s_telepathyServicePrefix)) {
        return;
    }

    if (newOwner.isEmpty()) {
        removeService(oldOwner, name);
    } else {
        addService(newOwner, name);
    }
}

void MainWindow::serviceNameFetchFinished(QDBusPendingCallWatcher *callWatcher)
{
    m_initialNameListReceived = true;

    QDBusPendingReply<QStringList> reply = *callWatcher;
    if (reply.isError()) {
        qDebug() << Q_FUNC_INFO << reply.error();
        return;
    }

    callWatcher->deleteLater();

    QDBusConnectionInterface *interface = QDBusConnection::sessionBus().interface();

    Q_FOREACH (const QString &service, reply.value()) {
        if (!service.startsWith(s_telepathyServicePrefix)) {
            continue;
        }

        const QDBusReply<QString> owner = interface->serviceOwner(service);

        onServiceOwnerChanged(service, /* old owner */ QString(), owner);
    }
}

void MainWindow::onProcessStatusChanged(TelepathyProcess::Status status, TelepathyProcess::Error error)
{
    Q_UNUSED(error)

    TelepathyProcess *process = qobject_cast<TelepathyProcess*>(sender());
    if (!process) {
        qDebug() << Q_FUNC_INFO << "Invalid slot call";
        return;
    }

    if (status == TelepathyProcess::Ready) {
        DebugMessageView *cmDebugMessageView = new DebugMessageView(this);
        cmDebugMessageView->setTelepathyProcess(process);

        QString tabText = process->names().first();
        m_ui.tabWidget->addTab(cmDebugMessageView, tabText);
    }
}

void MainWindow::addService(const QString &owner, const QString &service)
{
    Q_FOREACH (TelepathyProcess *process , m_processes) {
        if (process->ownerId() != owner) {
            continue;
        }
        if (process->names().contains(service)) {
            return;
        }
        process->addName(service);
        return;
    }

    TelepathyProcess *process = new TelepathyProcess(this);
    process->setOwnerId(owner);
    process->addName(service);
    connect(process, SIGNAL(statusChanged(TelepathyProcess::Status,TelepathyProcess::Error)), SLOT(onProcessStatusChanged(TelepathyProcess::Status,TelepathyProcess::Error)));
    m_processes.append(process);
}

void MainWindow::removeService(const QString &owner, const QString &service)
{
    Q_UNUSED(owner)
    Q_UNUSED(service)
}
